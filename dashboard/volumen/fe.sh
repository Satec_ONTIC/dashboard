#!/bin/bash

usage="./fe.sh file.pcap port"
# exporta un fichero pcap en netflow al proceso del puerto indicado
if [ $# -ne 2 ]
then
    echo "USAGE "$usage
    exit
fi
./flowexport_linux_x86_64.bin -a 5s -e 15s -nf5 127.0.0.1 $2 -f $1
name=$1
name=${name##*/}
name=$( echo $name | cut -d'.' -f 1)
echo -n $name > /dev/udp/localhost/9996
